/*********************************************************************
**********************************************************************

----------------------------------------------------------------------
File        : Joystick.h
Purpose     : Joystick manager include
----------------------------------------------------------------------
*/

#ifndef JOYSTICK_H            /* Make sure we only include it once */
#define JOYSTICK_H

#include "stm32f1xx_hal.h"
#include "usbd_hid.h"
#include "usb_device.h"

typedef struct{
	uint8_t		joyId;
	uint32_t	buttons;
	
	//uint16_t	throttle;
	uint16_t	rudder;
	
	uint8_t		hatSw1;
	
	uint16_t	xAxis;
	uint16_t	yAxis;

	uint16_t	xRotAxis;
	uint16_t	yRotAxis;

	} __packed JoyStateInt_t;

typedef union{
	JoyStateInt_t data;
	uint8_t raw[16];
	} __packed JoyState_t;

void JoystickUpdate(JoyState_t* joy);

#endif   /* JOYSTICK_H */

/*************************** End of file ****************************/
