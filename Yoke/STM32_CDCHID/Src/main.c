/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "adc.h"
#include "usb_device.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "usbd_hid.h"
#include "usbd_desc.h"
#include "usbd_composite.h"
#include "usb_descriptors.h"
#include "usbd_cdc_if.h"
#include "hd44780.h"
#include "Joystick.h"
#include "inlineDebounce.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
JoyState_t Joystick = {.data.joyId=1};
#define OVS_NUM 25

uint32_t rightPedalMax = 0, leftPedalMax = 0;
uint32_t rightPedalMin = 0xFFFFFFFF, leftPedalMin = 0xFFFFFFFF;


//data buffer form PC
#define BUFFER_SIZE 200
char rxData[BUFFER_SIZE];
uint32_t rxPtr = 0;
bool hasCommand = false;

//debouncers
debouncer_t functionDebounce = {.clickMin = 50};
debouncer_t startDebounce = {.clickMin = 50};
debouncer_t resetDebounce = {.clickMin = 50};

debouncer_t B1Debounce = {.clickMin = 50, .holdWait = 500, .holdRep = 100};
debouncer_t B2Debounce = {.clickMin = 50, .holdWait = 500, .holdRep = 100};
debouncer_t C1Debounce = {.clickMin = 50, .holdWait = 500, .holdRep = 25};
debouncer_t C2Debounce = {.clickMin = 50, .holdWait = 500, .holdRep = 25};

//timer/clock (mode 1)
char timeText[10];
uint32_t lastTime = 0;
uint32_t deltaTime = 0;
uint32_t measuredTime = 0;
bool measuring = false;
bool timer = false;

//flight and AP data
uint32_t heading, headingAP;
uint32_t altitude, altitudeAP;
int32_t verticalSpeed;
bool headingAPActive = false;
bool altitideAPActive = false;
bool APActive = false;

uint8_t APDispMode = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
int32_t map(int32_t x, int32_t in_min, int32_t in_max, int32_t out_min, int32_t out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
void reenumerate(){
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = GPIO_PIN_12;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//reenumerate
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
	HAL_Delay(500);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
	HAL_Delay(500);
}

void beginSerialHID() {
    USBD_Composite_Set_Descriptor(COMPOSITE_CDC_HID_DESCRIPTOR, COMPOSITE_CDC_HID_DESCRIPTOR_SIZE);

    USBD_Composite_Set_Classes(&USBD_CDC, &USBD_HID);

    in_endpoint_to_class[HID_EPIN_ADDR & 0x7F] = 1;

    reenumerate();
    USBD_Init(&hUsbDeviceFS, &FS_Desc_Composite, DEVICE_FS);
    USBD_RegisterClass(&hUsbDeviceFS, &USBD_Composite);
    USBD_CDC_RegisterInterface(&hUsbDeviceFS, &USBD_Interface_fops_FS);
    USBD_Start(&hUsbDeviceFS);
}



/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  //MX_USB_DEVICE_Init();
  MX_ADC1_Init();

  /* USER CODE BEGIN 2 */
  USB_Init();
  beginSerialHID();
  TM_HD44780_Init(8,2);

  HAL_ADCEx_Calibration_Start(&hadc1);

  uint8_t mode = 0;


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

	  for(uint8_t i = 0 ; i < 30 ; i++){
		  char c;
		  if(USB_GetChar(&c)){
			  if(c=='\r'){
				  rxData[rxPtr] = '\0';
				  hasCommand = true;
				  rxPtr = 0;
				  break;
			  }else if (c == '\n'){

			  }else{
				  rxData[rxPtr++] = c;
				  if(rxPtr >= BUFFER_SIZE){
					  rxPtr = 0;
				  }
			  }
		  }else{
			  break;
		  }
	  }

	  if(hasCommand){
		  hasCommand = false;
		  //USB_WriteString(rxData);
		  if(memcmp(rxData, "?d", 2) == 0){ // local FSX time
			  sprintf(timeText,"%c%c:%c%c:%c%c",rxData[2],rxData[3],rxData[4],rxData[5],rxData[6],rxData[7]);

		  }else if(memcmp(rxData, "#A", 2) == 0){ // Expert, LOCAL TIME, Seconds
			  uint32_t actTime = atoi(&rxData[2]);
			  if(lastTime > actTime){
				  deltaTime = 0;
			  }else{
				  deltaTime = actTime - lastTime;
			  }
			  lastTime = actTime;
		  }else if(memcmp(rxData, "=b", 2) == 0){ // AP Altitude
			  altitudeAP = atoi(&rxData[2]);
		  }else if(memcmp(rxData, "=d", 2) == 0){ // AP Heading
			  headingAP = atoi(&rxData[2]);
		  }else if(memcmp(rxData, "<D", 2) == 0){ // Altitude
			  altitude = atoi(&rxData[2]);
		  }else if(memcmp(rxData, "<J", 2) == 0){ // Heading
			  heading = atoi(&rxData[2]);
		  }else if(memcmp(rxData, "<L", 2) == 0){ // Vertcial speed
			  verticalSpeed = atoi(&rxData[2]);
		  }else if(memcmp(rxData, "=a", 2) == 0){ // AP Active
			  APActive = (rxData[2] == '1');
		  }else if(memcmp(rxData, "=j", 2) == 0){ // AP HDG active
			  headingAPActive = (rxData[2] == '1');
		  }else if(memcmp(rxData, "=k", 2) == 0){ // AP ALT active
			  altitideAPActive = (rxData[2] == '1');
		  }
	  }

	  JoystickUpdate(&Joystick);

	  Joystick.data.buttons = 0;

	  if(GPIO_Read(IO_MODE1)){
		  mode = 1;
	  }else if(GPIO_Read(IO_MODE2)){
		  mode = 2;
	  }else if(GPIO_Read(IO_MODE3)){
		  mode = 3;
	  }

	switch(mode){
	case 1:
	default:
		for(int i = 0 ; i < 8 ; i++){
		  if(GPIO_Read(i)){
			  Joystick.data.buttons |= (1UL<<i);
		  }
		}

		if(debounce(&functionDebounce,GPIO_Read(IO_FUNCTION),HAL_GetTick())) timer = !timer;
		if(debounce(&startDebounce,GPIO_Read(IO_START),HAL_GetTick())) measuring = !measuring;
		if(debounce(&resetDebounce,GPIO_Read(IO_RESET),HAL_GetTick())) measuredTime = 0;

		if(timer){
			TM_HD44780_Puts(0,0," TIMER ");
			char text[10];
			sprintf(text, "%02lu:%02lu:%02lu", measuredTime / 3600, (measuredTime%3600) / 60, measuredTime%60);
			TM_HD44780_Puts(0,1,text);
		}else{
			TM_HD44780_Puts(0,0," CLOCK ");
			TM_HD44780_Puts(0,1,timeText);
		}

		if(measuring){
			TM_HD44780_Puts(7,0,">");
		}else{
			TM_HD44780_Puts(7,0,":");
		}

		break;
	case 2:
		for(int i = 0 ; i < 8 ; i++){
		  if(i==2) i=6;
		  if(GPIO_Read(i)){
			  Joystick.data.buttons |= (1UL<<i);
		  }
		}

		if(debounceRep(&B1Debounce,GPIO_Read(IO_B1),HAL_GetTick())){
			USB_WriteString("B11\r\n");
		}
		if(debounceRep(&B2Debounce,GPIO_Read(IO_B2),HAL_GetTick())){
			USB_WriteString("B12\r\n");
		}
		if(debounceRep(&C1Debounce,GPIO_Read(IO_C1),HAL_GetTick())){
			USB_WriteString("A58\r\n");
		}
		if(debounceRep(&C2Debounce,GPIO_Read(IO_C2),HAL_GetTick())){
			USB_WriteString("A57\r\n");
		}

		if(debounce(&functionDebounce,GPIO_Read(IO_FUNCTION),HAL_GetTick())) APDispMode = (APDispMode + 1) % 5;
		if(debounce(&startDebounce,GPIO_Read(IO_START),HAL_GetTick())) APDispMode = 1;
		if(debounce(&resetDebounce,GPIO_Read(IO_RESET),HAL_GetTick())) APDispMode = 0;

		char text[10];

		switch(APDispMode){
		case 0:
		default://autopilot HDG and ALT
			sprintf(text, "AP   %03lu", headingAP);
			TM_HD44780_Puts(0,0,text);
			sprintf(text, "   %05lu", altitudeAP);
			TM_HD44780_Puts(0,1,text);
			break;
		case 1://actual HDG and ALT
			sprintf(text, "     %03lu", heading);
			TM_HD44780_Puts(0,0,text);
			sprintf(text, "   %05lu", altitude);
			TM_HD44780_Puts(0,1,text);
			break;
		case 2://ALT actual vs AP
			sprintf(text, "AP %05lu", altitudeAP);
			TM_HD44780_Puts(0,0,text);
			sprintf(text, "ALT%05lu", altitude);
			TM_HD44780_Puts(0,1,text);
			break;
		case 3://HDG actual vs AP
			sprintf(text, "AP   %03lu", headingAP);
			TM_HD44780_Puts(0,0,text);
			sprintf(text, "HDG  %03lu", heading);
			TM_HD44780_Puts(0,1,text);
			break;
		case 4://ALT and VS
			sprintf(text, "ALT%05lu", altitude);
			TM_HD44780_Puts(0,0,text);
			sprintf(text, "VS %+05ld", verticalSpeed);
			TM_HD44780_Puts(0,1,text);
			break;
		}

		break;
	case 3:
		for(int i = 0 ; i < 8 ; i++){
		  if(i==2) i=6;
		  if(GPIO_Read(i)){
			  Joystick.data.buttons |= (1UL<<i);
		  }
		}

		if(debounce(&B1Debounce,GPIO_Read(IO_B1),HAL_GetTick())){
			USB_WriteString("B05\r\n");
		}
		if(debounce(&B2Debounce,GPIO_Read(IO_B2),HAL_GetTick())){
			USB_WriteString("B05\r\n");
		}
		if(debounce(&C1Debounce,GPIO_Read(IO_C1),HAL_GetTick())){
			USB_WriteString("B04\r\n");
		}
		if(debounce(&C2Debounce,GPIO_Read(IO_C2),HAL_GetTick())){
			USB_WriteString("B04\r\n");
		}

		TM_HD44780_Puts(0,0,"AP: ");
		if(APActive){
			TM_HD44780_Puts(4,0," ON ");
		}else{
			TM_HD44780_Puts(4,0," OFF");
		}

		if(headingAPActive){
			TM_HD44780_Puts(4,1," HDG");
		}else{
			TM_HD44780_Puts(4,1," ---");
		}

		if(altitideAPActive){
			TM_HD44780_Puts(0,1,"ALT ");
		}else{
			TM_HD44780_Puts(0,1,"--- ");
		}
		break;
	}

	if(measuring) measuredTime += deltaTime;
	deltaTime = 0;

	  /*char text[10];
	  sprintf(text, " MODE:%d ", mode);
	  TM_HD44780_Puts(0,0,text);*/

	  uint8_t L = GPIO_Read(IO_HAT_L);
	  uint8_t R = GPIO_Read(IO_HAT_R);
	  uint8_t U = GPIO_Read(IO_HAT_UP);
	  uint8_t D = GPIO_Read(IO_HAT_DN);

	  if(U == 0 && R == 0 && D == 0 && L == 0){
		  Joystick.data.hatSw1 = 15;
	  }else if(U == 1 && R == 0 && D == 0 && L == 0){
		  Joystick.data.hatSw1 = 0;
	  }else if(U == 1 && R == 1 && D == 0 && L == 0){
		  Joystick.data.hatSw1 = 1;
	  }else if(U == 0 && R == 1 && D == 0 && L == 0){
		  Joystick.data.hatSw1 = 2;
	  }else if(U == 0 && R == 1 && D == 1 && L == 0){
		  Joystick.data.hatSw1 = 3;
	  }else if(U == 0 && R == 0 && D == 1 && L == 0){
		  Joystick.data.hatSw1 = 4;
	  }else if(U == 0 && R == 0 && D == 1 && L == 1){
		  Joystick.data.hatSw1 = 5;
	  }else if(U == 0 && R == 0 && D == 0 && L == 1){
		  Joystick.data.hatSw1 = 6;
	  }else if(U == 1 && R == 0 && D == 0 && L == 1){
		  Joystick.data.hatSw1 = 7;
	  }

	  Joystick.data.xAxis = ADC_GetBlockingOVS(ADC_CHANNEL_3, OVS_NUM, OVS_NUM);
	  Joystick.data.yAxis = ADC_GetBlockingOVS(ADC_CHANNEL_2, OVS_NUM, OVS_NUM);

	  //reserved Rudder and breaks from only 2 axis!
		uint32_t rightPedal = 4095 - ADC_GetBlockingOVS(ADC_CHANNEL_0, OVS_NUM, OVS_NUM);
		uint32_t leftPedal = 4095 - ADC_GetBlockingOVS(ADC_CHANNEL_1, OVS_NUM, OVS_NUM);

		rightPedalMax = MAX(rightPedalMax, rightPedal);
		rightPedalMin = MIN(rightPedalMin, rightPedal);

		leftPedalMax = MAX(leftPedalMax, leftPedal);
		leftPedalMin = MIN(leftPedalMin, leftPedal);

		rightPedal = map(rightPedal, rightPedalMin, rightPedalMax, 0, 4095);
		leftPedal = map(leftPedal, leftPedalMin, leftPedalMax, 0, 4095);

		uint32_t rudder = (4095/2) + rightPedal/2 - leftPedal/2;
		//Joystick.data.rudder = rudder;

		int32_t leftBreak = MIN(leftPedal, rightPedal) + map(rudder, 0, 4095, -(int32_t)MIN(leftPedal, rightPedal), MIN(leftPedal, rightPedal));
		int32_t rightBreak = MIN(leftPedal, rightPedal) - map(rudder, 0, 4095, -(int32_t)MIN(leftPedal, rightPedal), MIN(leftPedal, rightPedal));

		if(leftBreak < 0) leftBreak = 0;
		if(rightBreak < 0) rightBreak = 0;

		if(leftBreak > 4095) leftBreak = 4095;
		if(rightBreak > 4095) rightBreak = 4095;

		//Joystick.data.xRotAxis = leftBreak;
		//Joystick.data.yRotAxis = rightBreak;

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC|RCC_PERIPHCLK_USB;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
