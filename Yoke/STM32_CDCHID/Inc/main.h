/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define RUDDER1_Pin GPIO_PIN_0
#define RUDDER1_GPIO_Port GPIOA
#define RUDDER2_Pin GPIO_PIN_1
#define RUDDER2_GPIO_Port GPIOA
#define Y_Pin GPIO_PIN_2
#define Y_GPIO_Port GPIOA
#define X_Pin GPIO_PIN_3
#define X_GPIO_Port GPIOA
#define K_8_Pin GPIO_PIN_6
#define K_8_GPIO_Port GPIOA
#define K_7_Pin GPIO_PIN_7
#define K_7_GPIO_Port GPIOA
#define K_6_Pin GPIO_PIN_0
#define K_6_GPIO_Port GPIOB
#define K_5_Pin GPIO_PIN_1
#define K_5_GPIO_Port GPIOB
#define BOOT1_Pin GPIO_PIN_2
#define BOOT1_GPIO_Port GPIOB
#define K_4_Pin GPIO_PIN_10
#define K_4_GPIO_Port GPIOB
#define K_3_Pin GPIO_PIN_11
#define K_3_GPIO_Port GPIOB
#define K_2_Pin GPIO_PIN_12
#define K_2_GPIO_Port GPIOB
#define K_1_Pin GPIO_PIN_13
#define K_1_GPIO_Port GPIOB
#define HAT_7_Pin GPIO_PIN_14
#define HAT_7_GPIO_Port GPIOB
#define HAT_6_Pin GPIO_PIN_15
#define HAT_6_GPIO_Port GPIOB
#define HAT_5_Pin GPIO_PIN_8
#define HAT_5_GPIO_Port GPIOA
#define HAT_4_Pin GPIO_PIN_9
#define HAT_4_GPIO_Port GPIOA
#define HAT_3_Pin GPIO_PIN_10
#define HAT_3_GPIO_Port GPIOA
#define HAT_2_Pin GPIO_PIN_15
#define HAT_2_GPIO_Port GPIOA
#define HAT_1_Pin GPIO_PIN_3
#define HAT_1_GPIO_Port GPIOB
#define LCD_S3_Pin GPIO_PIN_4
#define LCD_S3_GPIO_Port GPIOB
#define LCD_S2_Pin GPIO_PIN_5
#define LCD_S2_GPIO_Port GPIOB
#define LCD_S1_Pin GPIO_PIN_6
#define LCD_S1_GPIO_Port GPIOB
#define LCD_CLK_Pin GPIO_PIN_7
#define LCD_CLK_GPIO_Port GPIOB
#define LCD_LATCH_Pin GPIO_PIN_8
#define LCD_LATCH_GPIO_Port GPIOB
#define LCD_DS_Pin GPIO_PIN_9
#define LCD_DS_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
