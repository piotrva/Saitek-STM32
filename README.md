STM32 CubeMX HAL Composite CDC and HID (Joystick)
======
Project based on informations found at: https://github.com/danieleff/Arduino_Core_STM32F1/tree/USB_HID_Composite

Project created in STM32 CubeMX for SW4STM32

1. Install [Virtual COM Port Driver](http://www.st.com/en/development-tools/stsw-stm32102.html)
2. Replace stmcdc.inf file, or add *&MI_00* after each VID & PID filtering sequence in your file before running *dpinst_xxx.exe*
3. Run *dpinst_xxx.exe*
4. Confirm when alert appears
5. If you have already drivers installed - uninstal them using device manager.


Feel free to modificate HID descriptor.


**WARNING** Regenerating project using CubeMX will overwrite some important files - I recommend using git tools to discard changes to any USB-related files