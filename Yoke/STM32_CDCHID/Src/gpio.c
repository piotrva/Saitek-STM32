/**
  ******************************************************************************
  * File Name          : gpio.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"
/* USER CODE BEGIN 0 */
#include <assert.h>

GpioInput_t gpios[18] = {
		 {.port = HAT_6_GPIO_Port, .pin = HAT_6_Pin}, //A1
		 {.port = HAT_7_GPIO_Port, .pin = HAT_7_Pin}, //A2
		 {.port = K_8_GPIO_Port, .pin = K_8_Pin}, //B1
		 {.port = K_3_GPIO_Port, .pin = K_3_Pin}, //B2
		 {.port = K_2_GPIO_Port, .pin = K_2_Pin}, //C1
		 {.port = K_5_GPIO_Port, .pin = K_5_Pin}, //C2
		 {.port = K_1_GPIO_Port, .pin = K_1_Pin}, //D
		 {.port = HAT_3_GPIO_Port, .pin = HAT_3_Pin}, //E

		 {.port = HAT_5_GPIO_Port, .pin = HAT_5_Pin}, //HAT UP
		 {.port = HAT_4_GPIO_Port, .pin = HAT_4_Pin}, //HAT L
		 {.port = HAT_2_GPIO_Port, .pin = HAT_2_Pin}, //HAT R
		 {.port = HAT_1_GPIO_Port, .pin = HAT_1_Pin}, //HAT DN

		 {.port = LCD_S1_GPIO_Port, .pin = LCD_S1_Pin}, //FUNCTION
		 {.port = LCD_S2_GPIO_Port, .pin = LCD_S2_Pin}, //START/STOP
		 {.port = LCD_S3_GPIO_Port, .pin = LCD_S3_Pin}, //RESET
		 {.port = K_7_GPIO_Port, .pin = K_7_Pin}, //MODE1
		 {.port = K_6_GPIO_Port, .pin = K_6_Pin}, //MODE2
		 {.port = K_4_GPIO_Port, .pin = K_4_Pin}, //MODE3
 };
/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */
bool GPIO_Read(uint16_t pin){
	assert(pin<18);
	return HAL_GPIO_ReadPin(gpios[pin].port, gpios[pin].pin) == GPIO_PIN_RESET;
}

/* USER CODE END 1 */

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pins : PAPin PAPin PAPin PAPin 
                           PAPin PAPin */
  GPIO_InitStruct.Pin = K_8_Pin|K_7_Pin|HAT_5_Pin|HAT_4_Pin 
                          |HAT_3_Pin|HAT_2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PBPin PBPin PBPin PBPin 
                           PBPin PBPin PBPin PBPin 
                           PBPin PBPin PBPin PBPin */
  GPIO_InitStruct.Pin = K_6_Pin|K_5_Pin|K_4_Pin|K_3_Pin 
                          |K_2_Pin|K_1_Pin|HAT_7_Pin|HAT_6_Pin 
                          |HAT_1_Pin|LCD_S3_Pin|LCD_S2_Pin|LCD_S1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = BOOT1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BOOT1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PBPin PBPin PBPin */
  GPIO_InitStruct.Pin = LCD_CLK_Pin|LCD_LATCH_Pin|LCD_DS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LCD_CLK_Pin|LCD_LATCH_Pin|LCD_DS_Pin, GPIO_PIN_RESET);

}

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
