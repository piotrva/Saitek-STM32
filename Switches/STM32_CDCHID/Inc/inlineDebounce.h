#ifndef __INLINEDEBOUNCE_H
#define __INLINEDEBOUNCE_H

#include "gpio.h"
#include <stdbool.h>

typedef enum {IDLE, DEBOUNCE, HOLD_WAIT, HOLD} debouncerState_t;

typedef struct{
	debouncerState_t state;
	uint32_t lastTime;
	uint32_t lastHold;
	bool lastState;
	uint32_t clickMin;
	uint32_t holdWait;
	uint32_t holdRep;
}debouncer_t;

bool debounce(debouncer_t* debouncer, bool actualState, uint32_t actualTicks);

bool debounceRep(debouncer_t* debouncer, bool actualState, uint32_t actualTicks);

#endif  // __INLINEDEBOUNCE_H
