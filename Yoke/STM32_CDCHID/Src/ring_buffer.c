/* Includes ------------------------------------------------------------------*/
#include <assert.h>
#include "ring_buffer.h"
#include "stm32f1xx_hal.h"

void CORE_ExitCriticalSection(void){
	__enable_irq();
}

void CORE_EnterCriticalSection(void){
	__disable_irq();
}

bool RingBuffer_Init(RingBuffer *ringBuffer, char *dataBuffer, size_t dataBufferSize) 
{
	assert(ringBuffer);
	assert(dataBuffer);
	assert(dataBufferSize > 0);
	
	if ((ringBuffer) && (dataBuffer) && (dataBufferSize > 0)) {
	  ringBuffer->dataBuffer = dataBuffer;
		ringBuffer->dataBufferSize = dataBufferSize;
		ringBuffer->dataBufferLen = 0;
		ringBuffer->tailIdx = 0;
		
		return true;
	}
	
	return false;
}

bool RingBuffer_Clear(RingBuffer *ringBuffer)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
		ringBuffer->dataBufferLen = 0;
		ringBuffer->tailIdx = 0;
		return true;
	}
	return false;
}

bool RingBuffer_IsEmpty(const RingBuffer *ringBuffer)
{
  assert(ringBuffer);	
	
	if (ringBuffer) {
		return ringBuffer->dataBufferLen == 0;
	}
	
	return true;
	
}

size_t RingBuffer_GetLen(const RingBuffer *ringBuffer)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
		return ringBuffer->dataBufferLen;
	}
	return 0;
	
}

size_t RingBuffer_GetCapacity(const RingBuffer *ringBuffer)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
		return ringBuffer->dataBufferSize;
	}
	return 0;	
}


bool RingBuffer_PutChar(RingBuffer *ringBuffer, char c)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
		CORE_EnterCriticalSection();
		if(ringBuffer->dataBufferLen == ringBuffer->dataBufferSize){
			CORE_ExitCriticalSection();
			return false;
		}
		unsigned int writeIndex = (ringBuffer->tailIdx + ringBuffer->dataBufferLen) % ringBuffer->dataBufferSize;
		ringBuffer->dataBuffer[writeIndex] = c;
		ringBuffer->dataBufferLen++;
		CORE_ExitCriticalSection();
		return true;
		
	}
	return false;
}

bool RingBuffer_GetChar(RingBuffer *ringBuffer, char *c)
{
	assert(ringBuffer);
	assert(c);
	
  if ((ringBuffer) && (c)) {
		CORE_EnterCriticalSection();
		if(ringBuffer->dataBufferLen == 0){
			CORE_ExitCriticalSection();
			return false;
		}
		*c = ringBuffer->dataBuffer[ringBuffer->tailIdx];
		ringBuffer->dataBufferLen--;
		ringBuffer->tailIdx = (ringBuffer->tailIdx + 1) % ringBuffer->dataBufferSize;
		CORE_ExitCriticalSection();
		return true;
	}
	return false;
}
