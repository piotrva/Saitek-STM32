/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "usb_device.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "usbd_hid.h"
#include "usbd_desc.h"
#include "usbd_composite.h"
#include "usb_descriptors.h"
#include "usbd_cdc_if.h"
#include "Joystick.h"
#include "inlineDebounce.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
JoyState_t Joystick = {.data.joyId=1};


//data buffer form PC
#define BUFFER_SIZE 200
char rxData[BUFFER_SIZE];
uint32_t rxPtr = 0;
bool hasCommand = false;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
int32_t map(int32_t x, int32_t in_min, int32_t in_max, int32_t out_min, int32_t out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
void reenumerate(){
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = GPIO_PIN_12;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//reenumerate
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
	HAL_Delay(500);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
	HAL_Delay(500);
}

void beginSerialHID() {
    USBD_Composite_Set_Descriptor(COMPOSITE_CDC_HID_DESCRIPTOR, COMPOSITE_CDC_HID_DESCRIPTOR_SIZE);

    USBD_Composite_Set_Classes(&USBD_CDC, &USBD_HID);

    in_endpoint_to_class[HID_EPIN_ADDR & 0x7F] = 1;

    reenumerate();
    USBD_Init(&hUsbDeviceFS, &FS_Desc_Composite, DEVICE_FS);
    USBD_RegisterClass(&hUsbDeviceFS, &USBD_Composite);
    USBD_CDC_RegisterInterface(&hUsbDeviceFS, &USBD_Interface_fops_FS);
    USBD_Start(&hUsbDeviceFS);
}


debouncer_t debouncersPos[20];
debouncer_t debouncersNeg[20];

const char stringToSendPos[20][40]={
		"C05\r\n", //Pilot heat
		"F10\r\n", //de-ice
		"E021\r\nE031\r\n", //fuel
		"Y021\r\n", //avionics (AVIONICS_MASTER_SET -> Experts)
		"E21\r\nE24\r\n", //ALT
		"E18\r\n", //BAT
		"C300\r\nC310\r\n", //Cowl
		"C461\r\n", //Panel
		"C421\r\n", //beacon
		"C411\r\n", //nav
		"C451\r\n", //strobe
		"C441\r\n", //taxi
		"C431\r\n", //landing
		"E15\r\n", //START
		"E14\r\n", //BOTH/ALL
		"E12\r\n", //R
		"E13\r\n", //L
		"E11\r\n", //OFF
		"C02\r\n", //gear DN
		"C01\r\n", //gear UP
};

const char stringToSendNeg[20][40]={
		"C06\r\n", //Pilot heat
		"F11\r\n", //de-ice
		"E020\r\nE030\r\n", //fuel
		"Y021\r\nY011\r\n", //avionics (AVIONICS_MASTER_SET & TOGGLE_AVIONICS_MASTER -> Experts)
		"E20\r\nE23\r\n", //ALT
		"E17\r\n", //BAT
		"C30100\r\nC31100\r\n", //Cowl
		"C460\r\n", //Panel
		"C420\r\n", //beacon
		"C410\r\n", //nav
		"C450\r\n", //strobe
		"C440\r\n", //taxi
		"C430\r\n", //landing
		"", //START
		"", //BOTH/ALL
		"", //R
		"", //L
		"", //OFF
		"", //gear DN
		"", //gear UP
};
/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */
	for(int i = 0 ; i < 20 ; i++){
		debouncersPos[i].clickMin = 50;
		debouncersPos[i].holdWait = 1000;
		debouncersPos[i].holdRep = 1000;

		debouncersNeg[i].clickMin = 50;
		debouncersNeg[i].holdWait = 1000;
		debouncersNeg[i].holdRep = 1000;
	}
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  //MX_USB_DEVICE_Init();

  /* USER CODE BEGIN 2 */
  USB_Init();
  beginSerialHID();


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

	  for(uint8_t i = 0 ; i < 30 ; i++){
		  char c;
		  if(USB_GetChar(&c)){
			  if(c=='\r'){
				  rxData[rxPtr] = '\0';
				  hasCommand = true;
				  rxPtr = 0;
				  break;
			  }else if (c == '\n'){

			  }else{
				  rxData[rxPtr++] = c;
				  if(rxPtr >= BUFFER_SIZE){
					  rxPtr = 0;
				  }
			  }
		  }else{
			  break;
		  }
	  }

	  if(hasCommand){
		  hasCommand = false;
		  //USB_WriteString(rxData);
		  if(memcmp(rxData, "<A", 2) == 0){ // gear nose (in detail)
			  uint32_t pos = atoi(&rxData[2]);
			  if(pos<50){
				  GPIO_Write(LED_N_G, false);
				  GPIO_Write(LED_N_R, false);
			  }else if(pos<100){
				  GPIO_Write(LED_N_G, false);
				  GPIO_Write(LED_N_R, true);
			  }else{
				  GPIO_Write(LED_N_G, true);
				  GPIO_Write(LED_N_R, false);
			  }
		  }else if(memcmp(rxData, "<B", 2) == 0){ // gear left (in detail)
			  uint32_t pos = atoi(&rxData[2]);
			  if(pos<50){
				  GPIO_Write(LED_L_G, false);
				  GPIO_Write(LED_L_R, false);
			  }else if(pos<100){
				  GPIO_Write(LED_L_G, false);
				  GPIO_Write(LED_L_R, true);
			  }else{
				  GPIO_Write(LED_L_G, true);
				  GPIO_Write(LED_L_R, false);
			  }
		  }else if(memcmp(rxData, "<C", 2) == 0){ // gear right (in detail)
			  uint32_t pos = atoi(&rxData[2]);
			  if(pos<50){
				  GPIO_Write(LED_R_G, false);
				  GPIO_Write(LED_R_R, false);
			  }else if(pos<100){
				  GPIO_Write(LED_R_G, false);
				  GPIO_Write(LED_R_R, true);
			  }else{
				  GPIO_Write(LED_R_G, true);
				  GPIO_Write(LED_R_R, false);
			  }
		  }
	  }

	  JoystickUpdate(&Joystick);

	  HAL_Delay(1);

	  Joystick.data.buttons = 0;
		for(int i = 0 ; i < 20 ; i++){
		  if(GPIO_Read(i)){
			  Joystick.data.buttons |= (1UL<<i);
			  if(debounceRep(&debouncersPos[i],true,HAL_GetTick())){
				  if(strlen(stringToSendPos[i])>0)USB_WriteString(stringToSendPos[i]);
			  }
			  debounceRep(&debouncersNeg[i],false,HAL_GetTick());
		  }else{
			  if(debounceRep(&debouncersNeg[i],true,HAL_GetTick())){
				  if(strlen(stringToSendNeg[i])>0)USB_WriteString(stringToSendNeg[i]);
			  }
			  debounceRep(&debouncersPos[i],false,HAL_GetTick());
		  }
		}

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
